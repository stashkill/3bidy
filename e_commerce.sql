-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 02 Apr 2022 pada 10.59 GG
-- Versi server: 10.4.17-MariaDB
-- Versi PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e_commerce`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(8) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `berat` int(60) NOT NULL,
  `stok` int(11) NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `image` text NOT NULL,
  `view` int(100) NOT NULL,
  `total_beli` int(50) NOT NULL,
  `waktu` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `harga`, `deskripsi`, `berat`, `stok`, `kategori`, `image`, `view`, `total_beli`, `waktu`) VALUES
(0, 'batubata', 0, 'dsd', 0, 0, 'sd', '<FileStorage: \'classE_commerce.jpg\' (\'image/jpeg\')>', 0, 0, '2022-03-23'),
(1, 'Bambi print mini Backpack', 299000, 'Bambi Ransel berbahan NYLON berukuran . . ', 750, 5, 'Tas', 'product1.jpg', 8, 0, NULL),
(2, 'Yoasobi T-Shirt Yoru ni Kakeru', 95000, 'Kaos t-shirt Yoasobi warna hitam nyaman dipakai, cocok untuk laki-laki dan nyaman dipakai saat beraktivitas', 200, 81, 'Tshirt', 'Yoasobi t-shirt.jpg', 26, 5, NULL),
(3, 'Sepatu Sneakers Formal Casual', 165000, 'Sepatu Sneakers  berbahan sintesis nyaman dipakai cocok untuk pria atau wanita', 1000, 24, 'Sepatu', 'product3.jpg', 2, 0, NULL),
(4, 'Celana jeans standar nevada', 117000, 'Celana panjang coklat jeans Nevada Ori ', 0, 18, 'Celana', 'celana nevada.jpeg', 2, 0, NULL),
(5, 'Kemeja merah lengan panjang', 75000, 'Kemeja merah lengan panjang pria nyaman dipakai cocok digunakan saat berpergian', 230, 17, 'Kemeja', 'kemeja.jpeg', 47, 2, NULL),
(6, 'Hilight - Brainwash Sharpgreen Tees', 60000, 'Amp up your t-shirt game with graphic tees from Hilight', 190, 12, 'Tshirt', '1.jpg', 3, 0, NULL),
(7, ' T-shirt Genshin Impact - Ganyu \" What is Reality \"', 140000, 'baju berbahan cotton combed 30s dengan sablon digital rubber print warna hitam', 250, 43, 'Tshirt', '2.jpg', 18, 2, NULL),
(8, 'Tshirt Pose Girl', 100000, ' Tshirt Pose Girl bahan PE Rayon fit to L. LD 90, panjang 57', 200, 50, 'Tshirt', '3.jpg', 9, 2, NULL),
(9, 'Kremlin T-shirt Anime Rumble - Gojo Satoru', 150000, 'DETAILS: - Regular Fit - Material 100% combed cotton 24s - Design Unisex - Dibuat dengan kualitas jahitan dan sablon terbaik - Sablon Plastisol', 250, 22, 'Tshirt', '4.1.jpg', 8, 1, NULL),
(10, 'Kaos Etnik batik - kaos jawa Half T Shirt - Mendung Setugel', 100000, 'Series \"MENDUNG SETUGEL\" - WISANGGENI . Short Sleeve - Half T-shirt Unisex type . Motif : Megamendung (Khas Cirebon) Warna : black & white Bahan : Cotton Combed 24s Sablon : superwhite Ink', 200, 50, 'Tshirt', '5.jpg', 36, 0, NULL),
(11, 'Rolex Submariner Homage STEELDIVE SD1953 Automatic 200m NH35 Seiko Diver Watch Original Strap', 1980000, 'This ever popular and enduring design makes the Submariner one of the most recognisable of watches. When people say they want to own a ‘good watch’ this is the watch they mean. Sapphire Crystal Glass Seiko NH35 Automatic Movement 120 Click Rotational Cera', 300, 5, 'Watch', 'a1.jpg', 2, 0, NULL),
(12, 'Jam tangan anak karakter', 13500, ' Jam tangan anak proyektor LED Suara', 150, 290, 'Watch', 'b1.jpg', 10, 10, NULL),
(13, 'BIDEN Jam Tangan Wanita', 145000, '-Nomor Model: R002 -Gerakan: Quartz &Digital -Style: Sport, Fashion, Cusual -Bentuk Kasus: Round -Band Jenis Bahan: Silicone -Case Bahan: Silicone -Dial Window Jenis Bahan: Kaca -Jenis Gesper: Gesper -Fitur: Tanggal Otomatis, Kronograf, Tangan Bercahaya K', 230, 78, 'Watch', 'c1.jpg', 4, 0, NULL),
(14, ' EIGER CARAVEL BASE BACKPACK', 307000, 'Caravel Base adalah backpack kecil yang dapat digunakan untuk beraktivitas sehari-hari. Tas berkapasitas 10 liter ini terbuat dari bahanyang dapat menahan air untuk sementara (water repellent). Tas ini dilengkapi juga dengan dua saku depan untuk menyimpan', 800, 60, 'Tas', 'd1.jpg', 7, 0, NULL),
(15, 'CIJI Tas Ransel / Backpack / Sekolah Anak Laki-Laki Karakter Superhero 3D', 99000, 'design ringan agar mudah dibawa saat berpergian - bahan kain ringan - bisa dijadikan handcarry ( sangat nyaman ) - pegangan tas yang di desain sesuai dengan nyaman dan kualitas ergonomis saat di angkat akan terasa nyaman - sangat ringan, seharian di pakai', 750, 58, 'Tas', 'e.jpg', 17, 1, NULL),
(16, 'MINISO Tas Belanja Serut Minions Series', 70000, '1、Ringan dan tampan dan berani 2、Desain fashion dan lucu 3、Bahan kanvas tebal bermutu tinggi. 4、Cocok untuk berbelanja dan jalan-jalan. 5、Hadiah sempurna untuk keluarga dan teman-teman.', 730, 60, 'Tas', 'f2.jpg', 19, 1, NULL),
(1130120, 'batuabtau', 23213, 'badsasds', 23, 29323, 'batybat', '<FileStorage: \'batu.jpg\' (\'image/jpeg\')>', 0, 0, NULL);

--
-- Trigger `barang`
--
DELIMITER $$
CREATE TRIGGER `waktu_barang` AFTER DELETE ON `barang` FOR EACH ROW BEGIN
	INSERT INTO barang SET
    waktu = NOW();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cart`
--

CREATE TABLE `cart` (
  `id_cart` int(12) NOT NULL,
  `barang` varchar(255) NOT NULL,
  `harga` int(50) NOT NULL,
  `quantity` int(20) NOT NULL,
  `kategori` varchar(30) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE `customer` (
  `NIK` int(50) NOT NULL,
  `uid` int(6) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `usia` varchar(20) NOT NULL,
  `tgl_lahir` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kota`
--

CREATE TABLE `kota` (
  `id_kota` varchar(12) NOT NULL,
  `kota` varchar(100) NOT NULL,
  `jarak` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kota`
--

INSERT INTO `kota` (`id_kota`, `kota`, `jarak`) VALUES
('101', 'Bandung', 5),
('105', 'Bekasi', 129),
('125', 'Jakarta', 151);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemesanan`
--

CREATE TABLE `pemesanan` (
  `no_pemesanan` int(8) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `penerima` varchar(60) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `no_telepon` varchar(15) NOT NULL,
  `kode_pos` int(8) NOT NULL,
  `waktu_pemesanan` timestamp NOT NULL DEFAULT current_timestamp(),
  `now_time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pemesanan`
--

INSERT INTO `pemesanan` (`no_pemesanan`, `nama_barang`, `username`, `penerima`, `alamat`, `kota`, `no_telepon`, `kode_pos`, `waktu_pemesanan`, `now_time`) VALUES
(30, 'BIDEN Jam Tangan Wanita', 'aqua', 'Dimas Tirtajasa', 'Jln Budi no 74', 'Bandung', '2147483647', 8341, '2022-02-07 03:34:05', '22-02-07 10:34'),
(31, 'BIDEN Jam Tangan Wanita', 'aqua', 'Dimas Tirtajasa', 'Jln Budi no 74', 'Bandung', '2147483647', 8341, '2022-02-07 03:35:00', '22-02-07 10:35'),
(32, 'BIDEN Jam Tangan Wanita', 'aqua', 'Dimas Tirtajasa', 'Jln Budi no 72', 'Bandung', '2147483647', 8341, '2022-02-07 03:35:32', '22-02-07 10:35'),
(33, 'Yoasobi T-Shirt Yoru ni Kakeru', 'aqua', 'Dimas Tirtajasa', 'Rumah saya', 'Bandung', '62378923', 4012, '2022-02-07 03:37:03', '22-02-07 10:37'),
(34, 'Yoasobi T-Shirt Yoru ni Kakeru', 'ABC', 'Rice Cooker', 'Rumah saya', 'Bandung', '62727372', 7273, '2022-02-07 04:52:57', '22-02-07 11:52'),
(35, 'Yoasobi T-Shirt Yoru ni Kakeru', 'ABC', 'Rice Cooker', 'Rumah saya', 'Bandung', '62727372', 7273, '2022-02-07 04:53:56', '22-02-07 11:53'),
(36, 'Yoasobi T-Shirt Yoru ni Kakeru', 'ABC', 'Rice Cooker', 'Rumah saya', 'Bandung', '62727372', 7273, '2022-02-07 04:54:35', '22-02-07 11:54'),
(37, 'Yoasobi T-Shirt Yoru ni Kakeru', 'ABC', 'Rice Cooker', 'Rumah saya', 'Bandung', '62727372', 7273, '2022-02-07 04:56:31', '22-02-07 11:56'),
(38, 'Yoasobi T-Shirt Yoru ni Kakeru', 'ABC', 'Rice Cooker', 'Rumah saya', 'Bandung', '62727372', 7273, '2022-02-07 04:57:01', '22-02-07 11:57'),
(39, 'Yoasobi T-Shirt Yoru ni Kakeru', 'ABC', 'Rice Cooker', 'Rumah saya', 'Bandung', '62727372', 7273, '2022-02-07 04:58:22', '22-02-07 11:58'),
(40, 'Yoasobi T-Shirt Yoru ni Kakeru', 'ABC', 'Rice Cooker', 'Rumah saya', 'Bandung', '62727372', 7273, '2022-02-07 05:00:37', '22-02-07 12:00'),
(41, 'Jam tangan anak karakter', 'ABC', 'Glass', 'Rumah Ortu', 'Bandung', '2147483647', 8272, '2022-02-07 08:57:30', '22-02-07 15:57'),
(42, 'Kaos Etnik batik - kaos jawa Half T Shirt - Mendung Setugel', 'ABC', 'Mouse', 'Bandung,Jln Budhi no18', 'Bandung', '628972782', 8291, '2022-02-07 10:36:42', '22-02-07 17:36'),
(43, 'Kaos Etnik batik - kaos jawa Half T Shirt - Mendung Setugel', 'ABC', 'Mouse', 'Bandung,Jln Budhi no18', 'Bandung', '628972782', 8291, '2022-02-07 10:36:48', '22-02-07 17:36'),
(44, 'Kaos Etnik batik - kaos jawa Half T Shirt - Mendung Setugel', 'ABC', 'Mouse', 'Bandung,Jln Budhi no18', 'Bandung', '628972782', 8291, '2022-02-07 10:37:27', '22-02-07 17:37'),
(45, 'Kaos Etnik batik - kaos jawa Half T Shirt - Mendung Setugel', 'ABC', 'Mouse', 'Bandung,Jln Budhi no18', 'Bandung', '628972782', 8291, '2022-02-07 10:38:05', '22-02-07 17:38'),
(46, 'Kaos Etnik batik - kaos jawa Half T Shirt - Mendung Setugel', 'ABC', 'Mouse', 'Bandung,Jln Budhi no18', 'Bandung', '628972782', 8291, '2022-02-07 10:43:30', '22-02-07 17:43'),
(47, 'Kaos Etnik batik - kaos jawa Half T Shirt - Mendung Setugel', 'ABC', 'Mouse', 'Bandung,Jln Budhi no18', 'Bandung', '628972782', 8291, '2022-02-07 10:45:54', '22-02-07 17:45'),
(48, 'Kaos Etnik batik - kaos jawa Half T Shirt - Mendung Setugel', 'ABC', 'Mouse', 'Bandung,Jln Budhi no18', 'Bandung', '628972782', 8291, '2022-02-07 10:54:13', '22-02-07 17:54'),
(49, 'Kaos Etnik batik - kaos jawa Half T Shirt - Mendung Setugel', 'ABC', 'Mouse', 'Bandung,Jln Budhi no18', 'Bandung', '628972782', 8291, '2022-02-07 10:55:18', '22-02-07 17:55'),
(50, 'Kaos Etnik batik - kaos jawa Half T Shirt - Mendung Setugel', 'ABC', 'Mouse', 'Bandung,Jln Budhi no18', 'Bandung', '628972782', 8291, '2022-02-07 10:58:27', '22-02-07 17:58'),
(51, 'Kaos Etnik batik - kaos jawa Half T Shirt - Mendung Setugel', 'ABC', 'Danang', 'Bandung Sukasari sarijadi blok 18', 'Bandung', '8922321', 7283, '2022-02-07 11:33:59', '22-02-07 18:33'),
(52, 'CIJI Tas Ransel / Backpack / Sekolah Anak Laki-Laki Karakter Superhero 3D', 'ABC', 'new', 'new', 'Bandung', '6228883', 8283, '2022-02-07 12:22:21', '22-02-07 19:22'),
(53, 'CIJI Tas Ransel / Backpack / Sekolah Anak Laki-Laki Karakter Superhero 3D', 'ABC', 'new', 'new', 'Jakarta', '6228883', 8283, '2022-02-07 12:23:31', '22-02-07 19:23'),
(54, 'CIJI Tas Ransel / Backpack / Sekolah Anak Laki-Laki Karakter Superhero 3D', 'ABC', 'new', 'new', 'Bandung', '6228883', 8283, '2022-02-07 12:24:55', '22-02-07 19:24'),
(55, 'CIJI Tas Ransel / Backpack / Sekolah Anak Laki-Laki Karakter Superhero 3D', 'ABC', 'new1', 'new2', 'Bandung', '6228883', 8283, '2022-02-07 12:29:07', '22-02-07 19:29'),
(56, 'CIJI Tas Ransel / Backpack / Sekolah Anak Laki-Laki Karakter Superhero 3D', 'ABC', 'new1', 'new2', 'Bandung', '6228883', 8283, '2022-02-07 12:30:42', '22-02-07 19:30'),
(57, 'CIJI Tas Ransel / Backpack / Sekolah Anak Laki-Laki Karakter Superhero 3D', 'ABC', 'new1', 'new2', 'Bandung', '6228883', 8283, '2022-02-07 12:31:27', '22-02-07 19:31'),
(58, 'CIJI Tas Ransel / Backpack / Sekolah Anak Laki-Laki Karakter Superhero 3D', 'ABC', 'new1', 'new2', 'Bandung', '6228883', 8283, '2022-02-07 12:32:24', '22-02-07 19:32'),
(59, 'CIJI Tas Ransel / Backpack / Sekolah Anak Laki-Laki Karakter Superhero 3D', 'ABC', 'new1', 'new2', 'Bandung', '6228883', 8283, '2022-02-07 12:32:36', '22-02-07 19:32'),
(60, 'Kaos Etnik batik - kaos jawa Half T Shirt - Mendung Setugel', 'ABC', 'batu', 'Bandung,Sukasari, Terusan sutami', 'Bandung', '23567', 2231, '2022-02-07 12:34:00', '22-02-07 19:34'),
(61, 'Kaos Etnik batik - kaos jawa Half T Shirt - Mendung Setugel', 'ABC', 'batu', 'Bandung,Sukasari, Terusan sutami', 'Bandung', '23567', 2231, '2022-02-07 12:35:06', '22-02-07 19:35'),
(62, 'Bambi print mini Backpack', 'aqua', 'Aqua', 'Bandung sukasari sarijadi terusan sutami', 'Bandung', '6283893', 2321, '2022-02-08 00:09:31', '22-02-08 07:09'),
(63, 'Bambi print mini Backpack', 'aqua', 'Aqua', 'Bandung sukasari sarijadi terusan sutami', 'Bandung', '6283893', 2321, '2022-02-08 00:10:30', '22-02-08 07:10'),
(64, 'Bambi print mini Backpack', 'aqua', 'Aqua', 'Bandung sukasari sarijadi terusan sutami', 'Bandung', '6283893', 2321, '2022-02-08 00:11:05', '22-02-08 07:11'),
(65, 'Kremlin T-shirt Anime Rumble - Gojo Satoru', 'aqua', 'Pandu', 'Bandung sukasari sarijadi terusan sutami', 'Bandung', '6282939', 8232, '2022-02-08 00:11:44', '22-02-08 07:11'),
(112, 'MINISO Tas Belanja Serut Minions Series', 'aqua', 'Nanda', 'Bandung Sukasari sarijadi blok 18 cibarunai', 'Bandung', '6289537644', 2321, '2022-02-16 05:34:04', '22-02-16 12:34'),
(113, 'MINISO Tas Belanja Serut Minions Series', 'Malik123', 'Malik', 'Bandung,Jln Budhi', 'Bandung', '6283922112', 2394, '2022-02-16 05:56:00', '22-02-16 12:56'),
(114, 'MINISO Tas Belanja Serut Minions Series', 'Malik123', 'Malik', 'Bandung,Jln Budhi', 'Bandung', '6283922112', 2394, '2022-02-16 05:57:35', '22-02-16 12:57'),
(115, 'MINISO Tas Belanja Serut Minions Series', 'Malik123', 'Malik', 'Bandung,Jln Budhi', 'Bandung', '6283922112', 2394, '2022-02-16 06:00:25', '22-02-16 13:00'),
(116, 'MINISO Tas Belanja Serut Minions Series', 'Malik123', 'Malik', 'Bandung,Jln Budhi', 'Bandung', '6283922112', 2394, '2022-02-16 06:01:32', '22-02-16 13:01'),
(117, 'MINISO Tas Belanja Serut Minions Series', 'Malik123', 'Malik', 'Bandung,Jln Budhi', 'Bandung', '6283922112', 2394, '2022-02-16 06:02:35', '22-02-16 13:02'),
(118, 'MINISO Tas Belanja Serut Minions Series', 'Malik123', 'Malik', 'Bandung,Jln Budhi', 'Bandung', '6283922112', 2394, '2022-02-16 06:03:56', '22-02-16 13:03'),
(119, 'MINISO Tas Belanja Serut Minions Series', 'Malik123', 'Malik', 'Bandung,Jln Budhi', 'Bandung', '6283922112', 2394, '2022-02-16 06:03:57', '22-02-16 13:03'),
(120, 'MINISO Tas Belanja Serut Minions Series', 'Malik123', 'Malik', 'Bandung,Jln Budhi', 'Bandung', '6283922112', 2394, '2022-02-16 06:04:02', '22-02-16 13:04'),
(121, 'MINISO Tas Belanja Serut Minions Series', 'Malik123', 'Malik', 'Bandung,Jln Budhi', 'Bandung', '6283922112', 2394, '2022-02-16 06:05:24', '22-02-16 13:05'),
(122, 'MINISO Tas Belanja Serut Minions Series', 'aqua', 'Dimas Tirtajasa', 'aasd', 'Bandung', '62323123', 2312, '2022-02-16 06:10:38', '22-02-16 13:10'),
(123, 'MINISO Tas Belanja Serut Minions Series', 'aqua', 'Dimas Tirtajasa', 'Bandung,Jln Budhi', 'Bandung', '628923123', 7231, '2022-02-16 06:14:42', '22-02-16 13:14'),
(124, 'Jam tangan anak karakter', 'aqua', 'Roboto', 'Bandung sarijadi blok s', 'Bandung', '089532314', 1323, '2022-02-22 04:05:45', '22-02-22 11:05'),
(125, 'Jam tangan anak karakter', 'aqua', 'Roboto', 'Bandung sarijadi blok s', 'Bandung', '089532314', 1323, '2022-02-22 04:06:19', '22-02-22 11:06'),
(126, 'BIDEN Jam Tangan Wanita', 'aqua', 'Nanda', 'Bandung Sukasari sarijadi blok 18 cibarunai', 'Bandung', '0895723839', 7281, '2022-02-22 15:45:51', '22-02-22 22:45'),
(127, 'BIDEN Jam Tangan Wanita', 'aqua', 'Nanda', 'Bandung Sukasari sarijadi blok 18 cibarunai', 'Bandung', '0895723839', 7281, '2022-02-22 15:49:33', '22-02-22 22:49'),
(128, 'Jam tangan anak karakter', 'owo123', 'Ronan', 'Bandung Sukasari sarijadi blok 18 cibarunai', 'Bandung', '6289232145', 8231, '2022-02-23 02:18:09', '22-02-23 09:18'),
(129, 'Bambi print mini Backpack', 'owo123', 'Melty', 'Bandung,Sukasari, Terusan sutami', 'Bandung', '62892828743', 3832, '2022-02-23 12:30:43', '22-02-23 19:30'),
(130, 'Bambi print mini Backpack', 'owo123', 'Melty', 'Bandung,Sukasari, Terusan sutami', 'Bandung', '62892828743', 3832, '2022-02-23 12:31:19', '22-02-23 19:31'),
(131, 'Bambi print mini Backpack', 'owo123', 'Melty', 'Bandung,Sukasari, Terusan sutami', 'Bandung', '62892828743', 3832, '2022-02-23 12:32:01', '22-02-23 19:32'),
(132, 'Bambi print mini Backpack', 'owo123', 'Melty', 'Bandung,Sukasari, Terusan sutami', 'Bandung', '62892828743', 3832, '2022-02-23 12:35:14', '22-02-23 19:35'),
(133, 'Kaos Etnik batik - kaos jawa Half T Shirt - Mendung Setugel', 'aqua', 'Izu', 'Bandung,Sukasari, Terusan sutami', 'Bandung', '628299454119', 7212, '2022-02-24 03:50:12', '22-02-24 10:50'),
(134, 'Yoasobi T-Shirt Yoru ni Kakeru', 'owo123', 'Bagus', 'Bandung Sukasari sarijadi blok 18 cibarunai', 'Bandung', '6289296544', 1317, '2022-02-28 03:35:00', '22-02-28 10:35'),
(135, 'Yoasobi T-Shirt Yoru ni Kakeru', 'aqua', 'Kazuma', 'Bandung Sukasari sarijadi blok 18 cibarunai', 'Bandung', '628923284', 1927, '2022-03-07 12:21:13', '22-03-07 19:21'),
(136, 'Kemeja merah lengan panjang', 'owo123', 'Sabun', 'bandung jalan budi', 'Bandung', '689023724', 7291, '2022-03-08 02:18:05', '22-03-08 09:18'),
(137, 'Jam tangan anak karakter', 'ikantuna000', 'an deude', 'jln.jalan no.00', 'Bandung', '0989098776', 40151, '2022-03-10 12:34:24', '22-03-10 19:34'),
(138, 'CIJI Tas Ransel / Backpack / Sekolah Anak Laki-Laki Karakter Superhero 3D', 'owo123', 'Sabun', 'Bandung sukasari sarijadi terusan sutami', 'Bandung', '628927387', 7281, '2022-03-11 01:45:31', '22-03-11 08:45'),
(139, 'Tshirt Pose Girl', 'owo123', 'sirin', 'Bandung sarijadi blok s', 'Bandung', '629872372', 7312, '2022-03-14 02:14:55', '22-03-14 09:14'),
(140, 'Kemeja merah lengan panjang', 'owo123', 'batu', 'Bandung sukasari sarijadi terusan sutami', 'Bandung', '08962321', 1321, '2022-03-23 03:07:12', '22-03-23 10:07'),
(141, 'Kemeja merah lengan panjang', 'owo123', 'batu', 'Bandung sukasari sarijadi terusan sutami', 'Bandung', '08962321', 1321, '2022-03-23 03:07:31', '22-03-23 10:07'),
(142, 'Kemeja merah lengan panjang', 'owo123', 'batu', 'Bandung sukasari sarijadi terusan sutami', 'Bandung', '08962321', 1321, '2022-03-23 03:12:09', '22-03-23 10:12'),
(143, ' T-shirt Genshin Impact - Ganyu \" What is Reality \"', 'owo123', 'hands', 'Bandung,Jln Budhi depan rumah sakit kasih bunda', 'Bandung', '08923124', 1231, '2022-03-23 03:27:44', '22-03-23 10:27'),
(144, 'Yoasobi T-Shirt Yoru ni Kakeru', 'owo123', 'DimasSimp', 'Rahasia', 'Bandung', '0897663231', 1020, '2022-03-23 03:37:20', '22-03-23 10:37'),
(145, 'Yoasobi T-Shirt Yoru ni Kakeru', 'aqua', 'batu bandung', 'Bandung sukasari sarijadi terusan sutami', 'Bandung', '089723214', 2353, '2022-03-24 07:24:02', '22-03-24 14:24'),
(146, 'Tshirt Pose Girl', 'owo123', 'Gojou', 'Jakarta, cari sendiri', 'Jakarta', '089762321', 5183, '2022-03-24 14:15:45', '22-03-24 21:15'),
(147, 'Kremlin T-shirt Anime Rumble - Gojo Satoru', 'owo123', 'demek', 'Jakarta, cari sendiri', 'Jakarta', '08231314', 1234, '2022-03-24 14:20:54', '22-03-24 21:20'),
(148, 'Kremlin T-shirt Anime Rumble - Gojo Satoru', 'owo123', 'demek', 'Jakarta, cari sendiri', 'Jakarta', '08231314', 1234, '2022-03-24 14:21:22', '22-03-24 21:21'),
(149, ' T-shirt Genshin Impact - Ganyu \" What is Reality \"', 'aqua', 'Dimas the Ruler', 'Bandung sukasari sarijadi terusan sutami', 'Jakarta', '08956235839', 8391, '2022-03-25 00:28:18', '22-03-25 07:28'),
(150, 'Yoasobi T-Shirt Yoru ni Kakeru', 'owo123', 'Robootoo', 'Bandung sukajadi jalan cipedes', 'Bandung', '08953768822', 1920, '2022-03-26 12:00:33', '22-03-26 19:00'),
(151, 'Yoasobi T-Shirt Yoru ni Kakeru', 'owo123', 'Robootoo', 'Bandung sukajadi jalan cipedes', 'Bandung', '08953768822', 1920, '2022-03-26 12:02:10', '22-03-26 19:02'),
(152, 'Yoasobi T-Shirt Yoru ni Kakeru', 'owo123', 'Robootoo', 'Bandung sukajadi jalan cipedes', 'Bandung', '08953768822', 1920, '2022-03-26 12:04:39', '22-03-26 19:04'),
(153, 'Yoasobi T-Shirt Yoru ni Kakeru', 'owo123', 'Robootoo', 'Bandung sukajadi jalan cipedes', 'Bandung', '08953768822', 1920, '2022-03-26 12:05:02', '22-03-26 19:05'),
(154, 'Yoasobi T-Shirt Yoru ni Kakeru', 'owo123', 'Robootoo', 'Bandung sukajadi jalan cipedes', 'Bandung', '08953768822', 1920, '2022-03-26 12:06:16', '22-03-26 19:06'),
(155, 'Yoasobi T-Shirt Yoru ni Kakeru', 'owo123', 'Robootoo', 'Bandung sukajadi jalan cipedes', 'Bandung', '08953768822', 1920, '2022-03-26 12:06:47', '22-03-26 19:06'),
(156, 'Yoasobi T-Shirt Yoru ni Kakeru', 'owo123', 'Robootoo', 'Bandung sukajadi jalan cipedes', 'Bandung', '08953768822', 1920, '2022-03-26 12:06:51', '22-03-26 19:06'),
(157, 'Yoasobi T-Shirt Yoru ni Kakeru', 'owo123', 'Robootoo', 'Bandung sukajadi jalan cipedes', 'Bandung', '08953768822', 1920, '2022-03-26 12:07:03', '22-03-26 19:07'),
(158, 'Yoasobi T-Shirt Yoru ni Kakeru', 'owo123', 'Robootoo', 'Bandung sukajadi jalan cipedes', 'Bandung', '08953768822', 1920, '2022-03-26 12:07:46', '22-03-26 19:07'),
(159, 'Yoasobi T-Shirt Yoru ni Kakeru', 'owo123', 'Robootoo', 'Bandung sukajadi jalan cipedes', 'Bandung', '08953768822', 1920, '2022-03-26 12:07:53', '22-03-26 19:07'),
(160, 'Yoasobi T-Shirt Yoru ni Kakeru', 'owo123', 'Robootoo', 'Bandung sukajadi jalan cipedes', 'Bandung', '08953768822', 1920, '2022-03-26 12:08:01', '22-03-26 19:08'),
(161, 'Yoasobi T-Shirt Yoru ni Kakeru', 'owo123', 'Robootoo', 'Bandung sukajadi jalan cipedes', 'Bandung', '08953768822', 1920, '2022-03-26 12:08:51', '22-03-26 19:08'),
(162, 'Yoasobi T-Shirt Yoru ni Kakeru', 'owo123', 'Ebony', 'Bandung sukajadi jalan cipedes', 'Bandung', '08953768822', 1920, '2022-03-26 12:32:52', '22-03-26 19:32'),
(163, 'Kemeja merah lengan panjang', 'bruce', 'Shadow Knight', 'Gotham city', 'Bogor', '0895325456', 1628, '2022-03-28 07:53:09', '22-03-28 14:53'),
(164, 'Kemeja merah lengan panjang', 'bruce', 'Shadow Knight', 'Gotham city', 'Bogor', '0895325456', 1628, '2022-03-28 07:53:14', '22-03-28 14:53'),
(165, 'Kemeja merah lengan panjang', 'bruce', 'Shadow Knight', 'Gotham city', 'Yogya', '0895325456', 1628, '2022-03-28 07:53:19', '22-03-28 14:53'),
(166, 'Kemeja merah lengan panjang', 'bruce', 'Shadow Knight', 'Gotham city', 'Jakarta', '0895325456', 1628, '2022-03-28 07:53:25', '22-03-28 14:53'),
(167, 'Kemeja merah lengan panjang', 'bruce', 'Shadow Knight', 'Gotham city', 'Bandung', '0895325456', 1628, '2022-03-28 07:54:14', '22-03-28 14:54'),
(168, 'Kemeja merah lengan panjang', 'bruce', 'Shadow Knight', 'Gotham city', 'Jakarta', '0895325456', 1628, '2022-03-28 07:54:22', '22-03-28 14:54'),
(169, 'Kemeja merah lengan panjang', 'bruce', 'Shadow Knight', 'Gotham city', 'Jakarta', '0895325456', 1628, '2022-03-28 07:55:56', '22-03-28 14:55'),
(170, 'Kemeja merah lengan panjang', 'bruce', 'Shadow Knight', 'Gotham city', 'Jakarta', '0895325456', 1628, '2022-03-28 07:57:11', '22-03-28 14:57'),
(171, 'Kemeja merah lengan panjang', 'bruce', 'Shadow Knight', 'Gotham city', 'Yogya', '0895325456', 1628, '2022-03-28 07:57:23', '22-03-28 14:57'),
(172, 'Kemeja merah lengan panjang', 'bruce', 'Shadow Knight', 'Gotham city', 'Yogya', '0895325456', 1628, '2022-03-28 07:57:28', '22-03-28 14:57'),
(173, 'Kemeja merah lengan panjang', 'bruce', 'Shadow Knight', 'Gotham city', 'Bandung', '0895325456', 1628, '2022-03-28 07:57:34', '22-03-28 14:57'),
(174, 'Kemeja merah lengan panjang', 'bruce', 'Shadow Knight', 'Gotham city', 'Bandung', '0895325456', 1628, '2022-03-28 07:57:49', '22-03-28 14:57'),
(175, 'Kemeja merah lengan panjang', 'bruce', 'Shadow Knight', 'Gotham city', 'Bandung', '0895325456', 1628, '2022-03-28 07:58:45', '22-03-28 14:58'),
(176, 'Kemeja merah lengan panjang', 'bruce', 'Shadow Knight', 'Gotham city', 'Bandung', '0895325456', 1628, '2022-03-28 08:02:41', '22-03-28 15:02'),
(177, 'Jam tangan anak karakter', 'aqua', 'makanan', 'Bandung sukasari sarijadi terusan sutami', 'Jakarta', '08923552', 1003, '2022-03-29 00:04:13', '22-03-29 07:04'),
(178, 'Jam tangan anak karakter', 'aqua', 'makanan', 'Bandung sukasari sarijadi terusan sutami', 'Bandung', '08923552', 1003, '2022-03-29 00:04:23', '22-03-29 07:04'),
(179, 'Jam tangan anak karakter', 'aqua', 'makanan', 'Bandung sukasari sarijadi terusan sutami', 'Bandung', '08923552', 1003, '2022-03-29 00:04:29', '22-03-29 07:04'),
(180, 'Jam tangan anak karakter', 'aqua', 'makanan', 'Bandung sukasari sarijadi terusan sutami', 'Bandung', '08923552', 1003, '2022-03-29 00:04:35', '22-03-29 07:04'),
(181, 'Jam tangan anak karakter', 'aqua', 'makanan', 'Bandung sukasari sarijadi terusan sutami', 'Jakarta', '08923552', 1003, '2022-03-29 00:04:46', '22-03-29 07:04'),
(182, 'Jam tangan anak karakter', 'aqua', 'logitech', 'Bandung sukajadi jalan cipedes', 'Bandung', '028384235', 2491, '2022-03-29 00:05:20', '22-03-29 07:05'),
(183, 'CIJI Tas Ransel / Backpack / Sekolah Anak Laki-Laki Karakter Superhero 3D', 'bruce', 'Marcel Wayne', 'Jl. Konoha no.27 ', 'Bandung', '0977771288273', 40164, '2022-03-29 01:13:06', '22-03-29 08:13'),
(184, ' T-shirt Genshin Impact - Ganyu \" What is Reality \"', 'owo123', 'test', 'Bandung sukajadi jalan cipedes', 'Bandung', '08972325', 3021, '2022-03-29 01:34:07', '22-03-29 08:34'),
(185, 'Kremlin T-shirt Anime Rumble - Gojo Satoru', 'owo123', 'test', 'Bandung sukajadi jalan cipedes', 'Bandung', '08293858', 2311, '2022-03-29 01:40:35', '22-03-29 08:40'),
(186, 'MINISO Tas Belanja Serut Minions Series', 'bruce', 'Bruce', 'Jl. Konoha no.27', 'Bandung', '099999272323', 40882, '2022-03-29 02:49:54', '22-03-29 09:49'),
(187, 'Jam tangan anak karakter', 'Avril', 'Raffa', 'Bandung sukajadi jalan cipedes', 'Bandung', '089723255', 7813, '2022-03-30 04:05:35', '22-03-30 11:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengiriman`
--

CREATE TABLE `pengiriman` (
  `code_pengiriman` varchar(50) NOT NULL,
  `code_transaksi` varchar(50) NOT NULL,
  `penerima` varchar(60) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `kota_tujuan` varchar(50) NOT NULL,
  `kode_pos` int(8) NOT NULL,
  `status_transaksi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengiriman`
--

INSERT INTO `pengiriman` (`code_pengiriman`, `code_transaksi`, `penerima`, `alamat`, `kota_tujuan`, `kode_pos`, `status_transaksi`) VALUES
('bidy102022032337', '100000214422032337', 'DimasSimp', 'Rahasia', 'Bandung', 1020, 'Pesanan sudah diterima'),
('bidy123122032327', '100000214322032327', 'hands', 'Bandung,Jln Budhi depan rumah sakit kasih bunda', 'Bandung', 1231, 'Pesanan sudah diterima'),
('bidy123422032420', '100000214722032420', 'demek', 'Jakarta, cari sendiri', 'Jakarta', 1234, 'Pesanan sedang dikirim'),
('bidy123422032421', '100000214822032421', 'demek', 'Jakarta, cari sendiri', 'Jakarta', 1234, 'Pesanan sudah diterima'),
('bidy131722022835', '100000213422022835', 'Bagus', 'Bandung Sukasari sarijadi blok 18 cibarunai', 'Bandung', 1317, 'Pesanan sedang dikirim'),
('bidy192022032632', '100000216222032632', 'Ebony', 'Bandung sukajadi jalan cipedes', 'Bandung', 1920, 'Pesanan sudah diterima'),
('bidy192722030721', '113522030721', 'Kazuma', 'Bandung Sukasari sarijadi blok 18 cibarunai', 'Bandung', 1927, 'Pesanan sudah diterima'),
('bidy235322032424', '114522032424', 'batu bandung', 'Bandung sukasari sarijadi terusan sutami', 'Bandung', 2353, 'Barang sedang dikemas'),
('bidy249122032905', '118222032905', 'logitech', 'Bandung sukajadi jalan cipedes', 'Bandung', 2491, 'Order confirmed'),
('bidy4015122031034', '100000613722031034', 'an deude', 'jln.jalan no.00', 'Bandung', 40151, 'Pesanan sudah diterima'),
('bidy4088222032949', '100000718622032949', 'Bruce', 'Jl. Konoha no.27', 'Bandung', 40882, 'Pesanan sudah diterima'),
('bidy781322033005', '100000818722033005', 'Raffa', 'Bandung sukajadi jalan cipedes', 'Bandung', 7813, 'Pesanan sudah diterima');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `code_transaksi` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `nama_barang` varchar(100) NOT NULL,
  `quantity` int(100) NOT NULL,
  `total` int(50) NOT NULL,
  `total_barang` int(50) NOT NULL,
  `penerima` varchar(60) NOT NULL,
  `no_telepon` varchar(15) NOT NULL,
  `status_pembayaran` varchar(50) NOT NULL,
  `status_transaksi` varchar(50) NOT NULL,
  `waktu_transaksi` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`code_transaksi`, `username`, `nama_barang`, `quantity`, `total`, `total_barang`, `penerima`, `no_telepon`, `status_pembayaran`, `status_transaksi`, `waktu_transaksi`) VALUES
('10000021012202165', 'owo123', 'MINISO Tas Belanja Serut Minions Series', 0, 85000, 0, 'Ronan', '62876442343', 'berhasil', 'sedang dikemas', '2022-02-25 05:02:50'),
('10000021022202165', 'owo123', 'MINISO Tas Belanja Serut Minions Series', 0, 85000, 0, 'Ronan', '62876442343', 'berhasil', 'siap dikirim', '2022-02-25 05:56:25'),
('10000021032202160', 'owo123', 'MINISO Tas Belanja Serut Minions Series', 0, 85000, 0, 'Ronan', '62876442343', 'berhasil', 'sedang dikemas', '2022-02-25 05:02:30'),
('10000021042202164', 'owo123', 'MINISO Tas Belanja Serut Minions Series', 0, 85000, 0, 'Ronan', '62876442343', 'berhasil', 'sedang dikemas', '2022-02-25 05:02:21'),
('10000021052202164', 'owo123', 'MINISO Tas Belanja Serut Minions Series', 0, 85000, 0, 'Ronan', '62876442343', 'berhasil', 'siap dikirim', '2022-02-25 05:58:29'),
('100000213422022835', 'owo123', 'Yoasobi T-Shirt Yoru ni Kakeru', 0, 110000, 0, 'Bagus', '6289296544', 'pending', 'sedang dikemas', '2022-02-28 03:35:01'),
('100000213622030818', 'owo123', 'Kemeja merah lengan panjang', 0, 90000, 0, 'Sabun', '689023724', 'pending', 'sedang dikemas', '2022-03-08 02:18:06'),
('100000213822031145', 'owo123', 'CIJI Tas Ransel / Backpack / Sekolah Anak Laki-Laki Karakter Superhero 3D', 0, 114000, 0, 'Sabun', '628927387', 'berhasil', 'Pesanan sudah diterima', '2022-03-11 02:54:36'),
('100000213922031414', 'owo123', 'Tshirt Pose Girl', 0, 115000, 0, 'sirin', '629872372', 'berhasil', 'Pesanan sedang dikirim', '2022-03-14 03:01:51'),
('100000214322032327', 'owo123', ' T-shirt Genshin Impact - Ganyu \" What is Reality \"', 0, 155000, 0, 'hands', '08923124', 'berhasil', 'Barang sedang dikemas', '2022-03-23 03:30:57'),
('100000214422032337', 'owo123', 'Yoasobi T-Shirt Yoru ni Kakeru', 0, 110000, 0, 'DimasSimp', '0897663231', 'pending', 'Barang sedang dikemas', '2022-03-23 03:38:05'),
('100000214622032415', 'owo123', 'Tshirt Pose Girl', 0, 142000, 0, 'Gojou', '089762321', 'pending', 'sedang dikemas', '2022-03-24 14:15:47'),
('100000214722032420', 'owo123', 'Kremlin T-shirt Anime Rumble - Gojo Satoru', 0, 192000, 0, 'demek', '08231314', 'pending', 'Order confirmed', '2022-03-24 14:20:55'),
('100000214822032421', 'owo123', 'Kremlin T-shirt Anime Rumble - Gojo Satoru', 0, 192000, 0, 'demek', '08231314', 'berhasil', 'Barang sedang dikemas', '2022-03-24 14:22:04'),
('100000215122032602', 'owo123', 'Yoasobi T-Shirt Yoru ni Kakeru', 4, 395000, 380000, 'Robootoo', '08953768822', 'pending', 'Barang sedang dikemas', '2022-03-26 12:02:11'),
('100000215222032604', 'owo123', 'Yoasobi T-Shirt Yoru ni Kakeru', 4, 395000, 380000, 'Robootoo', '08953768822', 'pending', 'Barang sedang dikemas', '2022-03-26 12:04:40'),
('100000215322032605', 'owo123', 'Yoasobi T-Shirt Yoru ni Kakeru', 4, 395000, 380000, 'Robootoo', '08953768822', 'pending', 'Barang sedang dikemas', '2022-03-26 12:05:04'),
('100000215422032606', 'owo123', 'Yoasobi T-Shirt Yoru ni Kakeru', 4, 395000, 380000, 'Robootoo', '08953768822', 'pending', 'Barang sedang dikemas', '2022-03-26 12:06:17'),
('100000215722032607', 'owo123', 'Yoasobi T-Shirt Yoru ni Kakeru', 4, 395000, 380000, 'Robootoo', '08953768822', 'pending', 'Barang sedang dikemas', '2022-03-26 12:07:03'),
('100000216022032608', 'owo123', 'Yoasobi T-Shirt Yoru ni Kakeru', 4, 395000, 380000, 'Robootoo', '08953768822', 'pending', 'Barang sedang dikemas', '2022-03-26 12:08:02'),
('100000216222032632', 'owo123', 'Yoasobi T-Shirt Yoru ni Kakeru', 4, 395000, 380000, 'Ebony', '08953768822', 'berhasil', 'Barang sedang dikemas', '2022-03-26 12:36:11'),
('100000218422032934', 'owo123', ' T-shirt Genshin Impact - Ganyu \" What is Reality \"', 1, 155000, 140000, 'test', '08972325', 'berhasil', 'Barang sedang dikemas', '2022-03-29 01:35:03'),
('100000218522032940', 'owo123', 'Kremlin T-shirt Anime Rumble - Gojo Satoru', 1, 165000, 150000, 'test', '08293858', 'berhasil', 'Barang sedang dikemas', '2022-03-29 01:41:06'),
('100000613722031034', 'ikantuna000', 'Jam tangan anak karakter', 0, 28500, 0, 'an deude', '0989098776', 'berhasil', 'barang sedang dikemas', '2022-03-10 12:39:52'),
('100000716722032854', 'bruce', 'Kemeja merah lengan panjang', 2, 165000, 150000, 'Shadow Knight', '0895325456', 'pending', 'Barang sedang dikemas', '2022-03-28 07:54:15'),
('100000716922032855', 'bruce', 'Kemeja merah lengan panjang', 2, 192000, 150000, 'Shadow Knight', '0895325456', 'pending', 'Barang sedang dikemas', '2022-03-28 07:55:57'),
('100000717022032857', 'bruce', 'Kemeja merah lengan panjang', 2, 192000, 150000, 'Shadow Knight', '0895325456', 'pending', 'Barang sedang dikemas', '2022-03-28 07:57:11'),
('100000717522032858', 'bruce', 'Kemeja merah lengan panjang', 2, 165000, 150000, 'Shadow Knight', '0895325456', 'pending', 'Barang sedang dikemas', '2022-03-28 07:58:45'),
('100000717622032802', 'bruce', 'Kemeja merah lengan panjang', 2, 165000, 150000, 'Shadow Knight', '0895325456', 'berhasil', 'Barang sedang dikemas', '2022-03-28 08:04:20'),
('100000718322032913', 'bruce', 'CIJI Tas Ransel / Backpack / Sekolah Anak Laki-Laki Karakter Superhero 3D', 1, 114000, 99000, 'Marcel Wayne', '0977771288273', 'berhasil', 'Barang sedang dikemas', '2022-03-29 01:18:09'),
('100000718622032949', 'bruce', 'MINISO Tas Belanja Serut Minions Series', 1, 85000, 70000, 'Bruce', '099999272323', 'berhasil', 'Barang sedang dikemas', '2022-03-29 02:52:14'),
('100000818722033005', 'Avril', 'Jam tangan anak karakter', 10, 150000, 135000, 'Raffa', '089723255', 'berhasil', 'Barang sedang dikemas', '2022-03-30 04:08:37'),
('113522030721', 'aqua', 'Yoasobi T-Shirt Yoru ni Kakeru', 0, 110000, 0, 'Kazuma', '628923284', 'berhasil', 'sudah diterima', '2022-03-08 13:14:28'),
('114522032424', 'aqua', 'Yoasobi T-Shirt Yoru ni Kakeru', 0, 110000, 0, 'batu bandung', '089723214', 'berhasil', 'Barang sedang dikemas', '2022-03-24 07:26:27'),
('114922032528', 'aqua', ' T-shirt Genshin Impact - Ganyu \" What is Reality \"', 0, 182000, 0, 'Dimas the Ruler', '08956235839', 'berhasil', 'Barang sedang dikemas', '2022-03-25 00:30:28'),
('117722032904', 'aqua', 'Jam tangan anak karakter', 2, 69000, 27000, 'makanan', '08923552', 'berhasil', 'Barang sedang dikemas', '2022-03-29 01:39:05'),
('118222032905', 'aqua', 'Jam tangan anak karakter', 2, 42000, 27000, 'logitech', '028384235', 'berhasil', 'Barang sedang dikemas', '2022-03-29 01:38:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `uid` int(7) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `no_telepon` varchar(20) NOT NULL,
  `foto_profile` text NOT NULL,
  `role` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`uid`, `username`, `password`, `nama_lengkap`, `email`, `no_telepon`, `foto_profile`, `role`) VALUES
(1, 'aqua', 'aries500', 'kamiaqua', 'aqua1@gmail.com', '628999999', 'profile.png', 'user'),
(999995, 'dimas_penjualan', '123456', 'Dimas The Bidy CEO', 'Dimasbandung@gmail.com', '628999999', 'profile.png', 'operator_penjualan'),
(999996, 'Dimas_transaksi', '123456', 'operator_transaksi', 'DimasBandung@gmail.com', '628732132', 'profile.png', 'operator_transaksi'),
(999997, 'Dimas_pengiriman', '123456', 'Dimas_pengiriman', 'Bandung123@gmail.com', '628793232', 'profile.png', 'admin_pengiriman'),
(999998, 'Dimas_barang', '123456', 'Dimas Bandung Raya', 'DimasRPL@gmail.com', '62822923', 'profile.png', 'admin_barang'),
(999999, 'error401', 'no permissions', 'error404', 'nopermissions@gmail.com', '111111111', 'profile.png', 'super'),
(1000000, 'ABC', 'aries500', 'BatuBaterai', 'drycell@gmail.com', '2147483647', '<FileStorage: \'profile.png\' (\'image/png\')>', 'user'),
(1000001, 'Leqram', 'Aries500Empire0', 'Leqram', 'leqram552@gmail.com', '2147483647', '<FileStorage: \'profile.png\' (\'image/png\')>', 'user'),
(1000002, 'owo123', 'aries500', 'Ronan Kanaj Wijaya', 'owo@gmail.com', '628762312', 'profile.png', 'user'),
(1000003, 'Malik123', 'aries500', 'Malik Malin', 'Malin@gmail.com', '622938192', '<FileStorage: \'profile.png\' (\'image/png\')>', 'user'),
(1000004, 'Santoso', 'aries500', 'Santoso Setyadji', 'setyadji@gmail.com', '6289232145', '<FileStorage: \'profile.png\' (\'image/png\')>', 'kurir'),
(1000005, 'AbdulPangestu123', 'aries500', 'abdul pangestu', 'pangestu444@gmail.com', '6289232132', '<FileStorage: \'profile.png\' (\'image/png\')>', 'kurir'),
(1000006, 'ikantuna000', 'Aries500', 'Crist Lev', 'greget999@gmail.com', '0983829928', '<FileStorage: \'profile.png\' (\'image/png\')>', 'user'),
(1000007, 'bruce', 'batman123', 'Bruce Wayne', 'SayaKaya@gmail.com', '0999999999', '<FileStorage: \'profile.png\' (\'image/png\')>', 'user'),
(1000008, 'Avril', '123456', 'Avril', 'Avril@gmail.com', '089623746', '<FileStorage: \'profile.png\' (\'image/png\')>', 'user');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indeks untuk tabel `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id_cart`);

--
-- Indeks untuk tabel `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`NIK`);

--
-- Indeks untuk tabel `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id_kota`);

--
-- Indeks untuk tabel `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`no_pemesanan`);

--
-- Indeks untuk tabel `pengiriman`
--
ALTER TABLE `pengiriman`
  ADD PRIMARY KEY (`code_pengiriman`),
  ADD KEY `code_transaksi` (`code_transaksi`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`code_transaksi`),
  ADD KEY `status_transaksi` (`status_transaksi`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `cart`
--
ALTER TABLE `cart`
  MODIFY `id_cart` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pemesanan`
--
ALTER TABLE `pemesanan`
  MODIFY `no_pemesanan` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `uid` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000009;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `pengiriman`
--
ALTER TABLE `pengiriman`
  ADD CONSTRAINT `pengiriman_ibfk_1` FOREIGN KEY (`code_transaksi`) REFERENCES `transaksi` (`code_transaksi`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
